Experimenting with smooth heap structure

In the first step this project has a few known heap structures created in python such that we can count comparisons and links. Then we implement a new structure called the smooth heap.
In theory that structuce is supposed to be equally well organized as the classical heaps. In this thesis im experimenting with different sets of data to find out wether smooth heap has a future.

Task list:
1. Implement different heap variants (high level comparasion and link counting)
2. compare various workloads with test data sets and a target of roughly 100k
3. Implement smooth heap
4. experiment with various datasets and heap variants
5. write thesis

heap variant operations:
1. extract minimum
2. insert
3. decrease key
4. meld/merge

abstract data type for heaps

operations on heaps

todos:
random, increasing, decreasing

note: to link 2 heaps after an extract min i have been using meld_internal, so i can use meld_internal_stable now to meld those heaps stable instead of unstable

sqlite implementation, visualizing data with chart.js

thesis link: https://de.overleaf.com/project/5d305590f1f0626cd908189f