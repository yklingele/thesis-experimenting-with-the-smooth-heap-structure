### Pseudo Code heap variants

#### Pairing Heap LTR
- insert
```python
if root < new node:
    root left child becomes right sibling of new node
    new node becomes left child of root
else:
    root becomes left child of new node
    new node becomes new root
```
- extract_min
```
collect all children of the root
pair them together from left to right (ignore last node if amount uneven)
meld all pairs
meld all resulting trees in one accumulation step
update the root
```
- meld
```python
if A < B:
    A left child becomes right sibling of B
    B becomes left child of A
else:
    B left child becomes right sibling of A
    A becomes left child of B
```
- decrease_key
```
find parent of the key to decrease
lower the value of the key
remove link between parent and the key
meld key (and its tree) with the root
```
#### Pairing Heap RTL
- extract_min
```
collect all children of the root
pair them together from right to left (ignore last node if amount uneven)
meld all pairs
meld all resulting trees in one accumulation step
update the root
```

#### Pairing Multipass LTR
- extract_min
```
collect all children of the root
pair them together from left to right (ignore last node if amount uneven)
meld all pairs
pair the resulting nodes again left to right
meld all pairs
continue until there is only one node left
update the root
```

Small notes on how the differences apply in the program directly:
LTR -> RTL appends the nodes in the list nodes_to_meld in reverse order
[left-middle-right] -> [right-middle-left]
The rest of the code applies the exact same way

LTR -> Multipass no accumulation step in the extract min, instead repeated pairing until there is just one element left to pair

Unstable -> Stable has a key change if the second node given in the meld function is larger than the first
that results in the larger node becoming the right most instead of the left child (if the smaller node already has a child/children)
The rest of the code applies the exact same way

RTL -> RTL Stable
since the list we create for the nodes_to_meld is in reverse we also need to put them in the meld function in reverse order. that way the right node "stays" on the right and so does the left one