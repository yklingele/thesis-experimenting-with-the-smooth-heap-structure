# Basic initialization for a Heap with a root node
class PairHeapRTL:
    def __init__(self, value):
        self.root_id = 1
        # Each node in the dictionary has the information of the value, leftmost child,
        # right sibling and if its the rightmost Node
        self.heap = {1: [value, None, None, True]}
        self.next_id = 2
        self.comparison_counter = 0
        # one link includes all pointer/root updates involved in that link action
        self.link_counter = 0

    def insert_node(self, value):
        # Case 1 root is smaller than the new node
        self.comparison_counter += 1
        self.link_counter += 1
        # New node becomes the left child of the root
        if self.heap[self.root_id][0] < value:
            # Case 1.1 root does not have a left child (no child at all)
            if self.heap[self.root_id][1] is None:
                self.heap[self.root_id][1] = self.next_id
                self.heap[self.next_id] = [value, None, None, True]
                self.next_id += 1
            # Case 1.2 root does have a left child
            else:
                self.heap[self.next_id] = [value, None, self.heap[self.root_id][1], False]
                self.heap[self.root_id][1] = self.next_id
                self.next_id += 1
        # Case 2 root is larger than the new node
        # Root becomes the left child of the new node
        else:
            self.heap[self.next_id] = [value, self.root_id, None, True]
            self.root_id = self.next_id
            self.next_id += 1

    def insert_nodes(self, list_of_nodes):
        for i in list_of_nodes:
            self.insert_node(i)

    def extract_min(self):
        # Case 1 root is the only node in the heap
        if self.heap[self.root_id][1] is None and self.heap[self.root_id][2] is None:
            del self.heap[self.root_id]
            print("Heap empty")
            return
        # Case 2 left child of root has no right sibling and becomes the new root
        if self.heap[self.heap[self.root_id][1]][2] is None:
            temp = self.heap[self.root_id][1]
            del self.heap[self.root_id]
            self.root_id = temp
            return
        # Case 3 removing root leaves several nodes without parent
        nodes_to_meld = [self.heap[self.root_id][1]]
        # Repeat as long as there is still a right sibling
        while not self.heap[nodes_to_meld[0]][3]:
            nodes_to_meld.insert(0, self.heap[nodes_to_meld[0]][2])
        # Meld right to left once
        nodes_to_acc = []
        for i in range(0, len(nodes_to_meld)-1, 2):
            self.meld_internal(nodes_to_meld[i], nodes_to_meld[i+1])
            # pointer check to find out which node is the parent
            if self.heap[nodes_to_meld[i]][1] == nodes_to_meld[i+1]:
                nodes_to_acc.append(nodes_to_meld[i])
            else:
                nodes_to_acc.append(nodes_to_meld[i+1])
        # Append last node, if there is no pair for every node
        if (len(nodes_to_meld) % 2) == 1:
            nodes_to_acc.append(nodes_to_meld[-1])
        while len(nodes_to_acc) > 1:
            self.meld_internal(nodes_to_acc[0], nodes_to_acc[1])
            # pointer check to find out which node is the parent
            if self.heap[nodes_to_acc[0]][1] == nodes_to_acc[1]:
                del nodes_to_acc[1]
            else:
                del nodes_to_acc[0]
        del self.heap[self.root_id]
        self.root_id = nodes_to_acc[0]

    def meld_internal(self, A, B):
        # 2 separated heaps in the same heap class, using their roots to meld
        self.comparison_counter += 1
        self.link_counter += 1
        # Case 1 A is larger than B, B becomes the root
        if self.heap[A][0] > self.heap[B][0]:
            # Case 1.1 B does not have a left child (no child at all)
            if self.heap[B][1] is None:
                self.heap[A][2:4] = [None, True]
                self.heap[B][1:4] = [A, None, True]
            # Case 1.2 B does have a left child, node1 becomes the new left child
            else:
                self.heap[A][2:4] = [self.heap[B][1], False]
                self.heap[B][1:4] = [A, None, True]
        # Case 2 A is smaller than B, A becomes the root
        else:
            # Case 2.1 A does not have a left child (no child at all)
            if self.heap[A][1] is None:
                self.heap[B][2:4] = [None, True]
                self.heap[A][1:4] = [B, None, True]
            # Case 2.2 A does have a left child, B becomes the new left child
            else:
                self.heap[B][2:4] = [self.heap[A][1], False]
                self.heap[A][1:4] = [B, None, True]

    def decrease_key(self, key_id, amount):
        # decrease value of the key by amount
        self.heap[key_id][0] -= amount
        # if the root gets decreased there is no need to update anything
        if self.root_id == key_id:
            return
        # find the parent or sibling node of the key and remove the link
        for parent_list in self.heap.items():
            if parent_list[1][1] == key_id:
                # remove key pointer from parent and add right sibling of node as left child if there is one
                self.heap[parent_list[0]][1] = self.heap[key_id][2]
            elif parent_list[1][2] == key_id:
                # remove key pointer from parent and add right sibling of node as right sibling if there is one
                self.heap[parent_list[0]][2:4] = self.heap[key_id][2:4]
        # merge the heap with the decreased key and the linked nodes
        self.meld_internal(self.root_id, key_id)
        # pointer check to find out which one is the root node
        if self.heap[key_id][1] == self.root_id:
            self.root_id = key_id

    def link_counter(self):
        return self.link_counter

    def comparison_counter(self):
        return self.comparison_counter

    def print_values(self):
        print("Amount of comparisons:", self.comparison_counter)
        print("Amount of links:", self.link_counter)

        # Print root value
        print('['+str(self.heap[self.root_id][0])+']')

        # Print root children values
        current_node_id = self.heap[self.root_id][1]
        list_of_parent_ids = [current_node_id]
        list_of_values = [self.heap[current_node_id][0]]

        # iterate until there is no right sibling
        while not self.heap[current_node_id][3]:
            list_of_parent_ids.append(self.heap[current_node_id][2])
            list_of_values.append(self.heap[self.heap[current_node_id][2]][0])
            current_node_id = self.heap[current_node_id][2]
        print(list_of_values)

        # Loop print the other rows with a list of lists
        next_list_of_parent_ids = []
        while not list_of_parent_ids == []:
            list_of_values = []
            # iterate all possible parents (iterate a complete row)
            for i in list_of_parent_ids:
                # check if the node has a left child
                if self.heap[i][1] is not None:
                    current_node_id = self.heap[i][1]
                    # iterate as long as the node is not the rightmost node in the row
                    while not self.heap[current_node_id][3]:
                        # add child to new row list
                        next_list_of_parent_ids.append(current_node_id)
                        # add value to the list of values printed out later
                        list_of_values.append(self.heap[current_node_id][0])
                        # set current node to right sibling
                        current_node_id = self.heap[current_node_id][2]
                    next_list_of_parent_ids.append(current_node_id)
                    list_of_values.append(self.heap[current_node_id][0])
                # print all values with no newline at the end
                print(list_of_values, end='')
                list_of_values = []
            # print a newline for new row
            print()
            # reset new row variable
            list_of_parent_ids = next_list_of_parent_ids
            next_list_of_parent_ids = []

    def find_id(self, value):
        return_ids = []
        for dict_list in self.heap.items():
            if dict_list[1][0] == value:
                return_ids.append(dict_list[0])
        print(return_ids)
