from PairHeapLTR import PairHeapLTR
from PairHeapRTL import PairHeapRTL
from PairHeapMultipass import PairHeapMulti
from PairHeapLTRStable import PairHeapLTRStable
from PairHeapRTLStable import PairHeapRTLStable
from PairHeapMultipassStable import PairHeapMultiStable
from SmoothHeap import SmoothHeap
from SmoothHeapUnstable import SmoothHeapUnstable

import random
import math
import matplotlib.pyplot as plt

"""
a = SmoothHeapUnstable(20)

a.insert_nodes([11, 15, 18, 2, 8, 3, 5, 4, 13, 1, 7, 6, 9, 14])
a.extract_min()
a.extract_min()
a.print_values()

#a.print_values()
"""
"""
amount_of_nodes_list = [100, 200, 300]
results_ysmooth_stable = []

for amount_of_nodes in amount_of_nodes_list:
    randomized_list = []
    for i in range(amount_of_nodes):
        randomized_list.append(i)
    random.shuffle(randomized_list)

    smoothheap = SmoothHeap(randomized_list[0])
    del randomized_list[0]

    smoothheap.insert_nodes(randomized_list)

    for i in range(1, len(smoothheap.heap)-1, 1):
        print(smoothheap.heap[smoothheap.root_id][0])
        smoothheap.extract_min()
    results_ysmooth_stable.append(smoothheap.link_counter)

plt.plot(amount_of_nodes_list, results_ysmooth_stable, "c.-", linewidth=0.3, label="Smooth Stable")
plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
plt.show()
"""


random.seed(92623745)
amount_of_nodes_list = [5000, 10000, 15000, 20000, 25000]
results_yltr = []
results_yltr_stable = []
results_yrtl = []
results_yrtl_stable = []
results_smoothunstable = []
results_ymulti_stable = []
results_ysmooth = []

for amount_of_nodes in amount_of_nodes_list:
    randomized_list = []
    for i in range(amount_of_nodes):
        randomized_list.append(i)
    random.shuffle(randomized_list)

    pairheapltr = PairHeapLTR(randomized_list[0])
    pairheapltrstable = PairHeapLTRStable(randomized_list[0])
    pairheaprtl = PairHeapRTL(randomized_list[0])
    pairheaprtlstable = PairHeapRTLStable(randomized_list[0])
    pairheapmulti = PairHeapMulti(randomized_list[0])
    pairheapmultistable = PairHeapMultiStable(randomized_list[0])
    smoothheap = SmoothHeap(randomized_list[0])
    del randomized_list[0]

    pairheapltr.insert_nodes(randomized_list)
    pairheapltrstable.insert_nodes(randomized_list)
    pairheaprtl.insert_nodes(randomized_list)
    pairheaprtlstable.insert_nodes(randomized_list)
    pairheapmulti.insert_nodes(randomized_list)
    pairheapmultistable.insert_nodes(randomized_list)
    smoothheap.insert_nodes(randomized_list)

    for i in range(1, len(pairheapltr.heap), 1):
        pairheapltr.extract_min()
    results_yltr.append(pairheapltr.link_counter)

    for i in range(1, len(pairheapltrstable.heap), 1):
        pairheapltrstable.extract_min()
    results_yltr_stable.append(pairheapltrstable.link_counter)

    for i in range(1, len(pairheaprtl.heap), 1):
        pairheaprtl.extract_min()
    results_yrtl.append(pairheaprtl.link_counter)

    for i in range(1, len(pairheaprtlstable.heap), 1):
        pairheaprtlstable.extract_min()
    results_yrtl_stable.append(pairheaprtlstable.link_counter)

    for i in range(1, len(pairheapmulti.heap), 1):
        pairheapmulti.extract_min()
    results_smoothunstable.append(pairheapmulti.link_counter)

    for i in range(1, len(pairheapmultistable.heap), 1):
        pairheapmultistable.extract_min()
    results_ymulti_stable.append(pairheapmultistable.link_counter)

    for i in range(1, len(smoothheap.heap), 1):
        smoothheap.extract_min()
    results_ysmooth.append(smoothheap.link_counter)

plt.plot(amount_of_nodes_list, results_yltr, "b.-", linewidth=0.3, label="LTR")
plt.plot(amount_of_nodes_list, results_yltr_stable, "y.-", linewidth=0.3, label="LTR Stable")
plt.plot(amount_of_nodes_list, results_yrtl, "g.-", linewidth=0.3, label="RTL")
plt.plot(amount_of_nodes_list, results_yrtl_stable, "m.-", linewidth=0.3, label="RTL Stable")
plt.plot(amount_of_nodes_list, results_smoothunstable, "r.-", linewidth=0.3, label="Smooth Unstable")
plt.plot(amount_of_nodes_list, results_ysmooth, "c.-", linewidth=0.3, label="Smooth Stable")
plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
plt.show()

