# Basic initialization for a Heap with a root node
class SmoothHeap:
    def __init__(self, value):
        self.root_id = 1
        # Each node in the dictionary has the information of the value, leftmost child,
        # left sibling, right sibling and if its the rightmost Node (circular linking on the left/right sibling)
        self.heap = {1: [value, None, None, None, True]}
        self.next_id = 2
        self.comparison_counter = 0
        # one link includes all pointer/root updates involved in that link action
        self.link_counter = 0

    def insert_node(self, value):
        # Case 1 root is smaller than the new node
        self.comparison_counter += 1
        self.link_counter += 1
        # New node becomes the left child of the root
        if self.heap[self.root_id][0] < value:
            # Case 1.1 root does not have a left child (no child at all)
            if self.heap[self.root_id][1] is None:
                self.heap[self.root_id][1] = self.next_id
                # For consistency a single child is circularly linked to itself
                self.heap[self.next_id] = [value, None, self.next_id, self.next_id, True]
                self.next_id += 1
            # Case 1.2 root does have a left child
            else:
                self.heap[self.next_id] = [value, None, self.heap[self.heap[self.root_id][1]][2], self.heap[self.root_id][1], False]
                self.heap[self.heap[self.heap[self.root_id][1]][2]][3] = self.next_id
                self.heap[self.heap[self.root_id][1]][2] = self.next_id
                self.heap[self.root_id][1] = self.next_id
                self.next_id += 1
        # Case 2 root is larger than the new node
        # Root becomes the left child of the new node
        else:
            self.heap[self.next_id] = [value, self.root_id, None, None, True]
            self.heap[self.root_id][2:5] = [self.root_id, self.root_id, True]
            self.root_id = self.next_id
            self.next_id += 1

    def insert_nodes(self, list_of_nodes):
        for i in list_of_nodes:
            self.insert_node(i)

    def extract_min(self):
        # Case 1 root is the only node in the heap
        if self.heap[self.root_id][1] is None:
            del self.heap[self.root_id]
            print("Heap empty")
            return
        # Case 2 left child of root has no right sibling and becomes the new root
        if self.heap[self.heap[self.root_id][1]][4]:
            print(self.heap[self.root_id][0])
            temp = self.heap[self.root_id][1]
            del self.heap[self.root_id]
            self.root_id = temp
            return
        # Case 3 removing root leaves several nodes without parent
        nodes_to_meld = [self.heap[self.root_id][1]]
        corresponding_values = [self.heap[nodes_to_meld[0]][0]]
        index = 0
        # the rightmost node will be saved and used for the accumulation step later
        while not self.heap[nodes_to_meld[-1]][4]:
            nodes_to_meld.append(self.heap[nodes_to_meld[-1]][3])
            corresponding_values.append(self.heap[nodes_to_meld[-1]][0])
        while len(nodes_to_meld) > 1:
            # Case 3.1: index is on the leftmost node
            if index == 0:
                # Case 3.1.1: leftmost node is a local maximum
                self.comparison_counter += 1
                if corresponding_values[0] > corresponding_values[1]:
                    self.big_small_meld(nodes_to_meld[0], nodes_to_meld[1])
                    del nodes_to_meld[0]
                    del corresponding_values[0]
                # Case 3.1.2: leftmost node is not a local maximum
                else:
                    index += 1
            # Case 3.2: index is within list
            elif index < len(nodes_to_meld):
                # Case 3.2.1: at the index is a local maximum
                self.comparison_counter += 1
                if corresponding_values[index-1] > corresponding_values[index]:
                    # Case 3.2.1.1: local max gets linked to the right
                    if corresponding_values[index-2] < corresponding_values[index]:
                        self.big_small_meld(nodes_to_meld[index-1], nodes_to_meld[index])
                        del nodes_to_meld[index-1]
                        del corresponding_values[index-1]
                        index -= 1
                    # Case 3.2.1.2: local max gets linked to the left and forces another local max
                    else:
                        self.small_big_meld(nodes_to_meld[index-2], nodes_to_meld[index-1])
                        del nodes_to_meld[index-1]
                        del corresponding_values[index-1]
                        # due to the nature of the smooth heap, we know there is another local maximum now
                        # we decrease the comparison counter by 1 because of that knowledge
                        self.comparison_counter -= 1
                        # we also step back one step on the index
                        index -= 2
                # Case 3.2.2: at the index there is not a local maximum
                else:
                    index += 1
            # Case 3.3: the index reached the end, RTL accumulation
            else:
                while len(nodes_to_meld) > 1:
                    self.small_big_meld(nodes_to_meld[len(nodes_to_meld)-2], nodes_to_meld[len(nodes_to_meld)-1])
                    del nodes_to_meld[-1]
        del self.heap[self.root_id]
        self.root_id = nodes_to_meld[0]

    def meld_internal(self, A, B):
        # 2 separated heaps in the same heap class, using their roots to meld
        # stable
        self.comparison_counter += 1
        # Case 1 A is larger than B, B becomes the root
        if self.heap[A][0] > self.heap[B][0]:
            self.big_small_meld(A, B)
        # Case 2 B is larger than A, A becomes the root
        else:
            self.small_big_meld(A, B)

    def big_small_meld(self, big, small):
        self.link_counter += 1
        # Case 1.1 B does not have a left child
        if self.heap[small][1] is None:
            # root takes over left sibling of bigger node
            self.heap[small][1] = big
            self.heap[small][2] = self.heap[big][2]
            # bigger node sets itself to be the only child of the root node
            self.heap[big][2:5] = [big, big, True]
        # Case 1.2 B does have a left child, A becomes the new leftmost child
        else:
            # root takes over left sibling of bigger node
            self.heap[small][2] = self.heap[big][2]
            # bigger node takes over leftmost child as right sibling, and its left sibling as left sibling
            self.heap[big][2:5] = [self.heap[self.heap[small][1]][2], self.heap[small][1], False]
            # rightmost child takes over big as right sibling
            self.heap[self.heap[self.heap[small][1]][2]][3] = big
            # left child takes over big as left sibling
            self.heap[self.heap[small][1]][2] = big
            # root takes over big as new leftmost child
            self.heap[small][1] = big
        return small

    def small_big_meld(self, small, big):
        self.link_counter += 1
        # Case 2.1 A does not have a left child
        if self.heap[small][1] is None:
            # root takes over right sibling of bigger node and sets bigger node as leftmost child
            self.heap[small][1] = big
            self.heap[small][3:5] = [self.heap[big][3], self.heap[big][4]]
            # bigger node sets itself to be the only child of the root node
            self.heap[big][2:5] = [big, big, True]
        # Case 2.2 A does have a left child, B becomes the new rightmost child
        else:
            # root takes over right sibling of bigger node
            self.heap[small][3:5] = [self.heap[big][3], self.heap[big][4]]
            # bigger node takes over left sibling of left child of root node and leftmost child as right sibling
            self.heap[big][2:5] = [self.heap[self.heap[small][1]][2], self.heap[small][1], True]
            # left sibling of the bigger node takes over bigger node as right sibling
            self.heap[self.heap[self.heap[small][1]][2]][3:5] = [big, False]
            # leftmost child of root takes over bigger node as left sibling
            self.heap[self.heap[small][1]][2] = big
        return small

    def print_values(self):
        print("Amount of comparisons:", self.comparison_counter)
        print("Amount of links:", self.link_counter)

        # Print root value
        print('['+str(self.heap[self.root_id][0])+']')

        # Print root children values
        current_node_id = self.heap[self.root_id][1]
        list_of_parent_ids = [current_node_id]
        list_of_values = [self.heap[current_node_id][0]]

        # iterate until there is no right sibling
        while not self.heap[current_node_id][4]:
            list_of_parent_ids.append(self.heap[current_node_id][3])
            list_of_values.append(self.heap[self.heap[current_node_id][3]][0])
            current_node_id = self.heap[current_node_id][3]
        print(list_of_values)

        # Loop print the other rows with a list of lists
        next_list_of_parent_ids = []
        while not list_of_parent_ids == []:
            list_of_values = []
            # iterate all possible parents (iterate a complete row)
            for i in list_of_parent_ids:
                # check if the node has a left child
                if self.heap[i][1] is not None:
                    current_node_id = self.heap[i][1]
                    # iterate as long as the node is not the rightmost node in the row
                    while not self.heap[current_node_id][4]:
                        # add child to new row list
                        next_list_of_parent_ids.append(current_node_id)
                        # add value to the list of values printed out later
                        list_of_values.append(self.heap[current_node_id][0])
                        # set current node to right sibling
                        current_node_id = self.heap[current_node_id][3]
                    next_list_of_parent_ids.append(current_node_id)
                    list_of_values.append(self.heap[current_node_id][0])
                # print all values with no newline at the end
                print(list_of_values, end='')
                list_of_values = []
            # print a newline for new row
            print()
            # reset new row variable
            list_of_parent_ids = next_list_of_parent_ids
            next_list_of_parent_ids = []

    def find_id(self, value):
        return_ids = []
        for dict_list in self.heap.items():
            if dict_list[1][0] == value:
                return_ids.append(dict_list[0])
        print(return_ids)